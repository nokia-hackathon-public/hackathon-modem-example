create_clock -period 50MHz [get_ports {CLOCK_50_B5B}]

derive_pll_clocks

set_clock_groups -asynchronous -group [get_clocks {CLOCK_50_B5B}] \
-group [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] \
-group [get_clocks {altera_reserved_tck}]

set_output_delay -clock [get_clocks {CLOCK_50_B5B}] 0 [get_ports {UART_TX}]
set_input_delay -clock [get_clocks {CLOCK_50_B5B}]  0 [get_ports {UART_RX}]
set_output_delay -clock [get_clocks {CLOCK_50_B5B}] 0 [get_ports {I2C_SCL}]
set_output_delay -clock [get_clocks {CLOCK_50_B5B}] 0 [get_ports {I2C_SDA}]
set_input_delay -clock [get_clocks {CLOCK_50_B5B}]  0 [get_ports {I2C_SDA}]

set_input_delay -clock [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 0 [get_ports {AUD_ADCDAT}]
set_output_delay -clock [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 0 [get_ports {AUD_ADCLRCK}]
set_output_delay -clock [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 0 [get_ports {AUD_BCLK}]
set_output_delay -clock [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 0 [get_ports {AUD_DACDAT}]
set_output_delay -clock [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 0 [get_ports {AUD_DACLRCK}]
set_output_delay -clock [get_clocks {hackathon_base_inst|analog_ss|cclk_pll|audio_pll|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}] 0 [get_ports {AUD_XCK}]

set_output_delay -clock [get_clocks {altera_reserved_tck}] 0 [get_ports {altera_reserved_tdo}]
set_input_delay -clock [get_clocks {altera_reserved_tck}] 0 [get_ports {altera_reserved_tdi}]
set_input_delay -clock [get_clocks {altera_reserved_tck}] 0 [get_ports {altera_reserved_tms}]

