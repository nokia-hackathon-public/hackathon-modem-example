
`default_nettype none

module modem_wrapper(
input  wire clk,
input  wire reset,

input  wire  [7:0] mod_symbol_data,
input  wire  [7:0] mod_symbol_valid,
output wire        mod_symbol_ready,

output wire [15:0] mod_iq_data,
input wire         mod_iq_ready,

input wire  [15:0] dem_iq_data,
input wire         dem_iq_valid,

output wire  [7:0] dem_symbol_data,
output wire        dem_symbol_valid,
input  wire        dem_symbol_ready
);


modem_uart#(
.DIV(4)
) modem (
.clk              (clk),
.reset            (reset),
.mod_symbol_data  (mod_symbol_data),
.mod_symbol_valid (mod_symbol_valid),
.mod_symbol_ready (mod_symbol_ready),
.mod_iq_data      (mod_iq_data),
.mod_iq_valid     (),
.mod_iq_ready     (mod_iq_ready),
.dem_iq_data      (dem_iq_data),
.dem_iq_valid     (dem_iq_valid),
.dem_iq_ready     (),
.dem_symbol_data  (dem_symbol_data),
.dem_symbol_valid (dem_symbol_valid),
.dem_symbol_ready (dem_symbol_ready)
);

endmodule

`default_nettype wire
