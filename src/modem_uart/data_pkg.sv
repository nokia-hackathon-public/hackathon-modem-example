
package data_pkg;

  typedef struct packed {
    logic signed [15:0] re;
    logic signed [15:0] im;
  } IQ_16;
  
  function automatic IQ_16 multiply_IQ(input IQ_16 ina, input IQ_16 inb);
  logic signed [31:0] re;
  logic signed [31:0] im;  
  IQ_16 temp;
    re = (ina.re * inb.re - ina.im * inb.im);
    im = (ina.im * inb.re + ina.re * inb.im);
  $display(re,im);
  temp.re = re >> 14;
  temp.im = im >> 14;
  $display(temp.re,temp.im);
  multiply_IQ = temp;
  endfunction
  
  function automatic logic signed [15:0] multiply_Q(input logic [15:0] ina, input logic [15:0] inb);
    logic signed [31:0] mul;
    mul = $signed(ina) * $signed(inb);
    multiply_Q = mul >> 14;
  endfunction

endpackage
