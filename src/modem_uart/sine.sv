
module sine#(parameter N = 16)(
input logic clk,
input logic [$clog2(N)+1:0] angle,
output logic signed[15:0] out
);

localparam PI = 3.141592653589793238;

wire [$clog2(N)-1:0] qangle;
wire [1:0] qsel;

assign qangle = angle[$clog2(N)-1:0];
assign qsel = angle[$clog2(N)+1:$clog2(N)];

reg [15:0] lut_sin [N-1:0];

int i;
initial begin
  $readmemh("sin16.hex", lut_sin);
//  for(i=0;i<N;i=i+1)
//    lut_sin[i] = 2**14*$sin(2*PI*i/(4*N));
end

always@(posedge clk)
  case(qsel)
    2'b00:
      out <= lut_sin[qangle];
    2'b01:
      out <= lut_sin[N-qangle-1];
    2'b10:
      out <= -lut_sin[qangle];
    2'b11:
      out <= -lut_sin[N-qangle-1];
    endcase

endmodule

